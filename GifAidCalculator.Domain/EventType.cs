namespace GiftAidCalculator.Domain
{
    public enum EventType
    {
        /*Although "Unspecified" is not used in this project, it's good practice to supply a default/unspecified enum.
            This ensures if someone uses a default value unknowingly it does not result in unexpected behaviour*/
        NotSpecified = 0,
        Running,
        Swimming
    }
}