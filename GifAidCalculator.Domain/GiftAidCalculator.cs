﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GiftAidCalculator.Domain
{
    public class GiftAidCalculator : IGiftAidCalculator
    {
        private readonly decimal _taxRate;

        public GiftAidCalculator()
        {
            _taxRate = 20m;
        }
        public GiftAidCalculator(decimal taxRate)
        {
            _taxRate = taxRate;
        }

        public decimal CalculateGiftAidAmount(decimal donationAmount)
        {
            ThrowExceptionIfDonationAmountIsNotValid(donationAmount);

            decimal giftAidRatio = _taxRate / (100 - _taxRate);

            return Math.Round((donationAmount * giftAidRatio), 2, MidpointRounding.AwayFromZero);
        }

        public decimal CalculateGiftAidAmount(decimal donationAmount, Dictionary<EventType, decimal> supplements)
        {
            decimal giftAidWithNoSupplement = CalculateGiftAidAmount(donationAmount);
            var supplementsToAdd = new List<decimal>();
            foreach (var supplement in supplements)
            {
                supplementsToAdd.Add(CalculateSupplement(giftAidWithNoSupplement, supplement.Value));
            }

            return giftAidWithNoSupplement + supplementsToAdd.Sum(s => s);
        }

        private decimal CalculateSupplement(decimal giftAidAmountWithNoSupplement, decimal supplementPercentage)
        {
            return (giftAidAmountWithNoSupplement * (supplementPercentage / 100m));
        }

        private static void ThrowExceptionIfDonationAmountIsNotValid(decimal donationAmount)
        {
            if (donationAmount <= 0)
            {
                throw new ArgumentOutOfRangeException("donationAmount can not be zero or negative, actual value was " +
                                                      donationAmount);
            }
        }
    }
}
