﻿using System.Collections.Generic;

namespace GiftAidCalculator.Domain
{
    public interface IGiftAidCalculator
    {
        decimal CalculateGiftAidAmount(decimal donationAmount);
        decimal CalculateGiftAidAmount(decimal donationAmount, Dictionary<EventType,decimal> supplements);
    }
}
