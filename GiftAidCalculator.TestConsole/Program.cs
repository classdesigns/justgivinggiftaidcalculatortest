﻿using System;

namespace GiftAidCalculator.TestConsole
{
	class Program
	{
// ReSharper disable UnusedParameter.Local
		static void Main(string[] args)
// ReSharper restore UnusedParameter.Local
		{
			// Calc Gift Aid Based on Previous
			Console.WriteLine("Please Enter donation amount:");
		    decimal donationAmount;
            decimal.TryParse(Console.ReadLine(), out donationAmount);
            Console.WriteLine("Please Enter tax rate:");
		    decimal taxRate;
            decimal.TryParse(Console.ReadLine(), out taxRate);
		    if (IsDonationOrTaxInvalid(donationAmount, taxRate))
		    {
		        Console.WriteLine("Donation and TaxRate must both be positive numbers, please press a key to try again");
		        Console.ReadLine();
                return;
		    }

		    Console.WriteLine("Thanks, your GiftAid declaration has given the charity an extra £{0}",  GiftAidAmount(donationAmount, taxRate));
			Console.WriteLine("Press any key to exit.");
			Console.ReadLine();
		}

	    private static bool IsDonationOrTaxInvalid(decimal donationAmount, decimal taxRate)
	    {
	        return ((donationAmount <= 0) || (taxRate <= 0));
	    }

	    static decimal GiftAidAmount(decimal donationAmount, decimal taxRate)
		{
		    var calculator = new Domain.GiftAidCalculator(taxRate);

		    return calculator.CalculateGiftAidAmount(donationAmount);
		}
	}
}
