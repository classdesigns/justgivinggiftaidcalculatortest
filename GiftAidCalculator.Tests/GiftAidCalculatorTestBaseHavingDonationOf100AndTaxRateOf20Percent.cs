﻿using NUnit.Framework;

namespace GiftAidCalculator.Tests
{
    public class GiftAidCalculatorTestBaseHavingDonationOf100AndTaxRateOf20Percent : GiftAidCalculatorTestBase
    {
        [SetUp]
        public void Setup()
        {
            GiftAidCalculator = new Domain.GiftAidCalculator(20m);
        }

        public decimal DonationOf100 { get { return 100m; }}
    }
}