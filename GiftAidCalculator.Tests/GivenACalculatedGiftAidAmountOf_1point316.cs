﻿using GiftAidCalculator.Tests;
using NUnit.Framework;

// ReSharper disable CheckNamespace
namespace GivenACalculatedGiftAidAmountOf_1point316
// ReSharper restore CheckNamespace
{
    [TestFixture]
    public class WhenRounded : GiftAidCalculatorTestBase
    {
        [SetUp]
        public void Setup()
        {
            GiftAidCalculator = new GiftAidCalculator.Domain.GiftAidCalculator(25m);
        }
        [Test]
        public void ShouldEqual_1point32()
        {
            const decimal onePoint32 = 1.32m;
            
            decimal roundedGiftAidAmount = GiftAidCalculator.CalculateGiftAidAmount(3.948m);
            
            Assert.That(roundedGiftAidAmount, Is.EqualTo(onePoint32));
        }
    }
}
