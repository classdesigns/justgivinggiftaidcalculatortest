﻿using GiftAidCalculator.Tests;
using NUnit.Framework;

// ReSharper disable CheckNamespace
namespace GivenADonationOf_100
// ReSharper restore CheckNamespace
{
    /// <summary>
    /// Business rules and breakdown of calculation
    /// Formula [Donation Amount] * ( [TaxRate] / (100 - [TaxRate]) )
    /// Example [Donation Amount] is 100, [TaxRate] is 20
    /// 100 * (20 / (100 - 20)
    /// 100 * (20 / 80)
    /// 100 * 0.25
    /// EQUALS 25

    /// </summary>
    [TestFixture]
    // ReSharper disable InconsistentNaming
    public class WhenTheTaxRateIs_20Percent : GiftAidCalculatorTestBase
    // ReSharper restore InconsistentNaming
    {
        [SetUp]
        public void Setup()
        {
            GiftAidCalculator = new GiftAidCalculator.Domain.GiftAidCalculator();
        }

        [Test]
        public void ThenTheGiftAidAmountShouldBe_25()
        {
            const decimal expectedGiftAidAmountOf25 = 25m;
            decimal calculatedGiftAidAmount = GiftAidCalculator.CalculateGiftAidAmount(100m);

            Assert.That(calculatedGiftAidAmount, Is.EqualTo(expectedGiftAidAmountOf25));
        }
    }  
}
