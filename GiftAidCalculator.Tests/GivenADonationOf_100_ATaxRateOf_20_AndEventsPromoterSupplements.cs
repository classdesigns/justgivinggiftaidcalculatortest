﻿using System.Collections.Generic;
using GiftAidCalculator.Domain;
using GiftAidCalculator.Tests;
using NUnit.Framework;

// ReSharper disable CheckNamespace
namespace GivenADonationOf_100_ATaxRateOf_20_AndEventsPromoterSupplements
// ReSharper restore CheckNamespace
{
    [TestFixture]
// ReSharper disable InconsistentNaming
    public class WhenTheEventIs_Running : GiftAidCalculatorTestBaseHavingDonationOf100AndTaxRateOf20Percent
// ReSharper restore InconsistentNaming
    {
        [Test]
        public void ThenTheGiftAidShouldBeCalculatedAs_25_Plus_A5PercentSuppliment()
        {
            const decimal expectedTotal26Point25 = 26.25m;
            Dictionary<EventType, decimal> supplements = new SupplementBuilder()
                .WithRunningEventSupplement()
                .Build();

            decimal calculatedGiftAidWithSupplement = GiftAidCalculator.CalculateGiftAidAmount(DonationOf100, supplements);
           
            Assert.That(calculatedGiftAidWithSupplement, Is.EqualTo(expectedTotal26Point25));
        }
    }

    [TestFixture]
// ReSharper disable InconsistentNaming
    public class WhenTheEventIs_Swimming : GiftAidCalculatorTestBaseHavingDonationOf100AndTaxRateOf20Percent
// ReSharper restore InconsistentNaming
    {
        [Test]
        public void ThenTheGiftAidShouldBeCalculatedAs_25_Plus_A3PercentSupplement()
        {
            const decimal expectedTotal25Point75 = 25.75m;
            Dictionary<EventType, decimal> swimmingSupplement = new SupplementBuilder()
                                                                    .WithSwimmingEventSupplement()
                                                                    .Build();
           
            decimal calculatedGiftAidWithSupplement = GiftAidCalculator.CalculateGiftAidAmount(DonationOf100, swimmingSupplement);
            
            Assert.That(calculatedGiftAidWithSupplement, Is.EqualTo(expectedTotal25Point75));
        }
    }
}
