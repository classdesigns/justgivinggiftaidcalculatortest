﻿using System;
using GiftAidCalculator.Tests;
using NUnit.Framework;

// ReSharper disable CheckNamespace
namespace GivenADonationThatIsNotAPositiveAmount
// ReSharper restore CheckNamespace
{
   
    [TestFixture]
    public class WhenTheDonationIsNegative : GiftAidCalculatorTestBase
    {
        [SetUp]
        public void Setup()
        {
            GiftAidCalculator = new GiftAidCalculator.Domain.GiftAidCalculator();
        }
        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ThenAnArgumentOutOfRangeExceptionIsThrown()
        {
            GiftAidCalculator.CalculateGiftAidAmount(-1m);
        }
    }

    [TestFixture]
    public class WhenTheDonationIsZero : GiftAidCalculatorTestBase
    {
        [SetUp]
        public void Setup()
        {
            GiftAidCalculator = new GiftAidCalculator.Domain.GiftAidCalculator();
        }

        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ThenAnArgumentOutOfRangeExceptionIsThrown()
        {
            GiftAidCalculator.CalculateGiftAidAmount(0m);
        }
    }
}
