﻿using GiftAidCalculator.Tests;
using NUnit.Framework;

// ReSharper disable CheckNamespace
namespace GivenATaxRateFromTheDataStore
// ReSharper restore CheckNamespace
{
    [TestFixture]
// ReSharper disable InconsistentNaming
    public class WhenTheTaxRateIs_25Percent_AndTheDonationIs_100 : GiftAidCalculatorTestBase
// ReSharper restore InconsistentNaming
    {
        [SetUp]
        public void Setup()
        {
            GiftAidCalculator = new GiftAidCalculator.Domain.GiftAidCalculator(25m);
        }

        [Test]
        public void ThenTheGiftAidAmountShouldBe_33point33()
        {
            const decimal expectedGiftAidAmount = 33.33m;
            
            decimal calculatedGiftAidAmount = GiftAidCalculator.CalculateGiftAidAmount(100m);
           
            Assert.That(calculatedGiftAidAmount, Is.EqualTo(expectedGiftAidAmount));
        }
    }
}
