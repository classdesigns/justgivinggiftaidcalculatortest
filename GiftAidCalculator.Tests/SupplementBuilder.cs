﻿using System.Collections.Generic;
using GiftAidCalculator.Domain;

namespace GiftAidCalculator.Tests
{
    public class SupplementBuilder
    {
        private Dictionary<EventType, decimal> _supplements = new Dictionary<EventType, decimal>();

        public SupplementBuilder WithSwimmingEventSupplement()
        {
            _supplements.Add(EventType.Swimming, 3m);
            return this;
        }

        public Dictionary<EventType, decimal> Build()
        {
            return _supplements;
        }

        public SupplementBuilder WithRunningEventSupplement()
        {
            _supplements.Add(EventType.Running, 5m);
            return this;
        }
    }
}